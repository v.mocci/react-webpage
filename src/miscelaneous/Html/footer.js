import React, { Component } from 'react';
import '../../App.css';

class Footer extends Component{
render(){
    return(
        <div className="footer">
            <p className="footerMention"> Made by: Vinicius Mocci, 2020 </p>
        </div>
    )
  }
}

export default Footer;