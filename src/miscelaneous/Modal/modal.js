import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const InitModal = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(true); // <- set this to 'false' to open the modal by clicking the button

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Saudações</ModalHeader>
        <ModalBody>
        Olá, Rafael e Amanda! <br></br>
        Confiram a pequena aplicação que fiz para mostrar a vocês. Está em React, mas na sessão "sobre" vocês podem conferir as demais tecnologias que utilizei.
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Ir para o site</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default InitModal;