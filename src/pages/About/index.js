import React, { Component } from 'react';
import Header from '../../miscelaneous/Html/header';
import Footer from '../../miscelaneous/Html/footer';
import { Container, Row, Col } from 'reactstrap';
import '../../App.css';

class About extends Component{
    render(){
         return(
        <div>
           <Header/>
                <Container>     
                <Row>
                       <Col xs="2" sm="4" className=""></Col>
                       <Col xs="8" sm="4" className="aboutInfoTitle"><p></p><h4>Sobre</h4></Col>
                       <Col xs="2" sm="4" className=""></Col>
                     </Row>
                     <Row>
                       <Col xs="2" sm="4" className=""></Col>
                            <Col xs="8" sm="4" className="aboutInfoData">
                                <p></p>
                                <p>Olá!</p> 
                                <p>Este projeto foi feito com as seguintes tecnologias:</p>
                                <ul>
                                <li>React.js/JavaScript;</li>
                                <li>HTML;</li>
                                <li>CSS;</li>
                                <li>API da TV Maze;</li>
                                <li>Versionamento via GitLab;</li>
                                </ul>
                                <p> Você pode conferir o código do projeto <a href="https://gitlab.com/v.mocci/react-webpage" target="_blank">clicando aqui</a></p>
                            </Col>
                       <Col xs="2" sm="4" className=""></Col>
                       <br></br>
                     </Row>
                </Container>
           <Footer/>
        </div>
    );
  };
};

export default About;