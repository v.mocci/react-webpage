import React, { Component } from 'react';
import api from '../../miscelaneous/Apis/api';
import '../../App.css';
import InitModal from '../../miscelaneous/Modal/modal';
import Header from '../../miscelaneous/Html/header';
import Footer from '../../miscelaneous/Html/footer';

class Main extends Component{

  state= {
    filmes: [],
  }

  async componentDidMount() {
    const results  = 'star%20wars'
    const response = await api.get(results);
    console.log(response.data)
    this.setState({ filmes: response.data});
  }
  
  render(){

    const { filmes } = this.state;
    return(
      <div>
        <InitModal/>
        <Header/>
        <h1> Resultados </h1>
        {console.log(filmes)}
        {filmes.map(filme => (
          <div className="films" key={filme.show.id}>
            <div className="">
            <h4 className="filmTitle">
              Título: {filme.show.name}
            </h4>
            <p>
              {filme.show.url}
            </p>
            {filme.show.premiered} <br></br>
            {filme.show.genres[1] + ", " + filme.show.genres[2]}
            <br></br> <br></br>
            </div>
            
          </div>
                  
        )
      )
    };
         <Footer/> 
      </div>
    );
  };
};

export default Main;